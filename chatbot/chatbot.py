# -*- coding: utf-8 -*-

import requests
import vk_api
from pony.orm import db_session
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from vk_api.utils import get_random_id
from datetime import date
import handlers
from generate_ticket import generate_ticket
from models import UserStates, Registration

try:
    import settings
except ImportError:
    exit('Нужно изменить файл settings.py.default на settings.py и изменить значения на свои')

import logging

log = logging.getLogger('Bot_logger')


def logging_info():
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s', '%Y-%m-%d %H:%M'))
    stream_handler.setLevel(logging.DEBUG)
    log.addHandler(stream_handler)

    file_handler = logging.FileHandler('bot.log', encoding='utf8')
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s', '%Y-%m-%d %H:%M:%S'))
    file_handler.setLevel(logging.DEBUG)
    log.addHandler(file_handler)

    log.setLevel(logging.INFO)


class ChatBot:
    """
    Echo bot для vk.com
    Use python3.7
    """

    FIVE_NEAR_DATE = []

    def __init__(self, token, group):
        """
        :param token: Уникальный/секретный токен. Генерируется в группе vk.com
        :param group: id группы в vk.com. Где найти? Пример: https://vk.com/clubXXXXXXXXX
        """
        self.token = token
        self.group_id = group
        self.vk_session = vk_api.VkApi(token=self.token)
        self.long_poll = vk_api.bot_longpoll.VkBotLongPoll(self.vk_session, self.group_id)
        self.api = self.vk_session.get_api()
        self.user_id_in_db = None
        self.text_to_send = ''

        log.info('Новая сессия')

    def run(self):
        """Запуск бота."""
        for event in self.long_poll.listen():
            try:
                self.on_event(event)
            except Exception as err:
                log.exception('Ошибка', err)

    @db_session
    def on_event(self, event):
        """
        Отправляет сообщение назад.
        :param event: VkBotMessageEvent
        :return: None
        """
        if event.type == VkBotEventType.MESSAGE_NEW:
            first_name, last_name, user_id, user_text = self.user_info(event)

            self.user_id_in_db = UserStates.get(user_id=str(user_id))

            if self.user_id_in_db is not None and self.user_id_in_db.step_name == 'step_9':
                # если пользователь в сценарии но ответить ему больше нечем
                self.user_not_in_scenario(user_text, first_name, user_id, last_name)

            elif self.user_id_in_db is not None:
                # если пользователь в сценарии
                self.user_in_scenario(user_id, user_text, first_name, last_name)

            else:
                # если пользователя нет в сценарии
                self.user_not_in_scenario(user_text, first_name, user_id, last_name)

            log.info(f'{first_name} {last_name} написал: {user_text}')
        else:
            log.debug(f'Эта функция пока не работает, {event.type}')

    def registration(self, first_name, last_name):
        """Запись пользователя в бд для регистрации"""
        Registration(
            city_out=self.user_id_in_db.city_out,
            city_in=self.user_id_in_db.city_in,
            date=self.user_id_in_db.date,
            name=f'{first_name} {last_name}',
            passengers=self.user_id_in_db.passengers)

    def send_text(self, user_id):
        """Отправка сообщения пользователю."""
        self.api.messages.send(
            user_id=user_id,
            random_id=get_random_id(),
            message=self.text_to_send
        )
        log.info(f'Бот ответил: {self.text_to_send}')

    def send_image(self):
        """
        Отправка изображение (билета) бользователя
        :return: None
        """
        # Запуск генератора билета
        file_like_img = generate_ticket(
            name=self.user_id_in_db.name,
            city_in=self.user_id_in_db.city_in,
            city_out=self.user_id_in_db.city_out,
            date=self.user_id_in_db.date,
            passengers=self.user_id_in_db.passengers,
            user_id=self.user_id_in_db.user_id)

        # Работа с vk.api. Плучение url для загрузки изображения, загрузка, отправка полльзователю
        upload_url = self.api.photos.getMessagesUploadServer()
        download_info = requests.post(
            upload_url['upload_url'], files={'photo': ('image.png', file_like_img, 'image/png')}).json()
        image_to_send = self.api.photos.saveMessagesPhoto(
            photo=download_info['photo'],
            server=download_info['server'],
            hash=download_info['hash'])[0]

        self.api.messages.send(
            peer_id=self.user_id_in_db.user_id,
            attachment=f'photo{image_to_send["owner_id"]}_{image_to_send["id"]}',
            random_id=get_random_id()
        )

    def user_info(self, event):
        """
        Обработка данных пользователя.
        :param event: VkBotMessageEvent
        :return: Возвращает данные пользователя, а именно, first_name, last_name, user_id, user_text
        """
        user_id = event.object['message']['from_id']
        user_text = event.object['message']['text']
        user_name = self.api.users.get(user_id=user_id)
        first_name = user_name[0]['first_name']
        last_name = user_name[0]['last_name']

        return first_name, last_name, user_id, user_text

    def start_scenario(self, scenario_name, user_id, first_name, last_name):
        """Добавление пользователя в сценарий self.user_states"""
        scenario = settings.SCENARIOS[scenario_name]
        first_step = scenario['first_step']
        self.text_to_send = scenario['steps'][first_step]['text']
        UserStates(
            user_id=str(user_id),
            scenario_name=scenario_name,
            step_name=first_step,
            name=f'{first_name} {last_name}',
            email='None',
            city_out='None',
            city_in='None',
            date='None',
            passengers='None',
            comment='None',
        )

    @db_session
    def user_in_scenario(self, user_id, user_text, first_name, last_name):
        """Пользователь в сценарии"""
        scenario_name = self.user_id_in_db.scenario_name
        state = settings.SCENARIOS[scenario_name]['steps']
        user_step = self.user_id_in_db.step_name

        handler_name = state[user_step]['handler']
        handler = getattr(handlers, handler_name)

        if handler(user_text):
            # если пользователь ввел верные данные
            context = handler(user_text)
            self.user_id_in_db.step_name = settings.SCENARIOS[scenario_name]['steps'][user_step]['next_step']
            user_step = self.user_id_in_db.step_name
            context_name = list(context.keys())
            context_value = list(context.values())
            setattr(self.user_id_in_db, context_name[0], context_value[0])

            name = self.user_id_in_db.name
            email = self.user_id_in_db.email
            city_out = self.user_id_in_db.city_out
            city_in = self.user_id_in_db.city_in
            date_in_db = self.user_id_in_db.date
            passengers = self.user_id_in_db.passengers
            comment = self.user_id_in_db.comment
            boolean = True

            if user_step == 'step_4':
                boolean = self.date_choice(user_text=user_text)

            elif user_step == 'step_9':
                self.registration(first_name, last_name)

            elif user_text.lower() == 'нет':
                self.user_id_in_db.delete()

                self.start_scenario(scenario_name, user_id, first_name, last_name)
                user_step = 'step_1'
                self.FIVE_NEAR_DATE.clear()

            if boolean is True:
                self.text_to_send = state[user_step]['text'].format(
                    name=name,
                    email=email,
                    city_out=city_out,
                    city_in=city_in,
                    date=date_in_db,
                    passengers=passengers,
                    comment=comment,
                    date_choice='\n'.join(self.FIVE_NEAR_DATE)
                )

            elif boolean is False:
                self.user_id_in_db.step_name = 'step_3'
        else:
            # если пользователь ввел неверные данные
            self.text_to_send = state[user_step]['failure_text'].format(city='\n'.join(settings.LIST_OF_FLIGHTS[0]))

        # удаление юзера из дб
        self.send_text(user_id=user_id)

        if self.user_id_in_db.step_name == 'step_9':
            self.send_image()
            self.user_id_in_db.delete()

    def date_choice(self, user_text):
        """Рефакторинг даты"""
        scenario_name = self.user_id_in_db.scenario_name
        step_name = self.user_id_in_db.step_name
        city_out = self.user_id_in_db.city_out
        city_in = self.user_id_in_db.city_in
        old_list_of_dates = settings.LIST_OF_FLIGHTS[0][city_out][city_in]
        date_in_list = []

        date_today = date.today()
        month = date_today.month
        old_list_of_dates_month = int(old_list_of_dates[0][3:5])
        difference = month - old_list_of_dates_month

        for i in old_list_of_dates:
            old_month = int(i[3:5])
            new_month = old_month + difference
            date_in_list.append(i[0:3] + str(new_month).zfill(2) + i[5:10])

        user_date = user_text.split('-')
        user_day = int(user_date[0])
        user_month = int(user_date[1])
        user_year = int(user_date[2])

        month_today = date.today()
        month = int(month_today.month)
        day = int(month_today.day)
        if user_month <= month and user_day < day:
            self.text_to_send = settings.SCENARIOS[scenario_name]['steps'][step_name]['failure_text']
            return False

        else:
            def date_checker(number):
                for index_date, date_in in enumerate(date_in_list):
                    date_split = date_in.split('-')
                    date_split_day = int(date_split[0])
                    date_split_month = int(date_split[1])
                    date_split_year = int(date_split[2])
                    if date_split_day == number and date_split_month == user_month and date_split_year == user_year:
                        return index_date

            for _ in range(10):
                index = date_checker(number=user_day)
                user_day += 1
                if index:
                    stop = 3
                    for num in range(-2, stop):
                        index_check = num + index
                        if 0 <= index_check <= len(date_in_list) - 1:
                            self.FIVE_NEAR_DATE.append(date_in_list[index_check])
                    break
            return True

    def user_not_in_scenario(self, user_text, first_name, user_id, last_name):
        """Пользователь не в сценарии"""
        for intent in settings.INTENTS:
            if any(token in user_text.lower() for token in intent['tokens']):
                if intent['answer']:
                    # если ответ найден
                    self.text_to_send = intent['answer'].format(name=first_name)
                else:
                    # если answer is None, запускаем сценарий
                    self.start_scenario(
                        scenario_name=intent['scenario'],
                        user_id=user_id,
                        first_name=first_name,
                        last_name=last_name,
                    )
                break
            else:
                self.text_to_send = settings.DEFAULT_ANSWER

        self.send_text(user_id=user_id)  # отправка сообщения пользователю


if __name__ == '__main__':
    logging_info()
    chat_bot = ChatBot(token=settings.TOKEN_ID, group=settings.GROUP_ID)
    chat_bot.run()
