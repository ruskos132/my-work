from copy import deepcopy
from unittest import TestCase
from unittest.mock import patch, Mock

from pony.orm import rollback, db_session
from vk_api.bot_longpoll import VkBotMessageEvent
from datetime import date
import settings
from chatbot import ChatBot
from generate_ticket import generate_ticket


def isolate_db(test_func):
    def wrapper(*args, **kwargs):
        with db_session:
            test_func(*args, **kwargs)
            rollback()
    return wrapper


def month():
    time = date.today()
    return str(time.month + 1).zfill(2)


class ChatBotTests(TestCase):

    RAW_EVENT = {
        'type': 'message_new', 'object':
            {
                'message': {'date': 1588533496, 'from_id': 12649692, 'id': 151, 'out': 0, 'peer_id': 12649692,
                            'text': 'привет', 'conversation_message_id': 77, 'fwd_messages': [], 'important': False,
                            'random_id': 0, 'attachments': [], 'is_hidden': False},
                'client_info': {'button_actions': ['text', 'vkpay', 'open_app', 'location', 'open_link'],
                                'keyboard': True, 'inline_keyboard': True, 'lang_id': 0}
            }, 'group_id': 194275369, 'event_id': 'ca2a67b7eedc49edb86449545e8d448252644455'}

    INPUTS = [
        'gkjhg',
        'Привет',
        'помоги',
        'Зарегистрируй меня',
        'Киев',
        'Москва',
        f'07-{month()}-2020',
        f'07-{month()}-2020',
        '1',
        'Просто так',
        'Да',
        '+79310000000',
    ]
    EXPECTED_OUTPUTS = [
        settings.DEFAULT_ANSWER,
        settings.INTENTS[0]['answer'].format(name='Костя'),
        settings.INTENTS[1]['answer'].format(name='Костя'),

        settings.SCENARIOS['registration']['steps']['step_1']['text'],
        settings.SCENARIOS['registration']['steps']['step_2']['text'],
        settings.SCENARIOS['registration']['steps']['step_3']['text'],
        settings.SCENARIOS['registration']['steps']['step_4']['text'].format(
            date_choice=f'01-{month()}-2020\n'
                        f'06-{month()}-2020\n'
                        f'08-{month()}-2020\n'
                        f'13-{month()}-2020\n'
                        f'15-{month()}-2020'),

        settings.SCENARIOS['registration']['steps']['step_5']['text'],
        settings.SCENARIOS['registration']['steps']['step_6']['text'],
        settings.SCENARIOS['registration']['steps']['step_7']['text'].format(
            city_in='Москва',
            city_out='Киев',
            date=f'07-{month()}-2020',
            passengers='1',
            comment='Просто так'

        ),
        settings.SCENARIOS['registration']['steps']['step_8']['text'],
        settings.SCENARIOS['registration']['steps']['step_9']['text'],

    ]
    USER_NAME = [
        {'id': 12649692,
         'first_name': 'Костя',
         'last_name': 'Суханкин',
         'is_closed': False,
         'can_access_closed': True}]

    def test_run(self):
        count = 5
        obj = {'a': 1}
        event = [obj] * count
        long_poll_mock = Mock(return_value=event)
        long_poll_listen_mock = Mock()
        long_poll_listen_mock.listen = long_poll_mock
        with patch('chatbot.vk_api.VkApi'):
            with patch('chatbot.vk_api.bot_longpoll.VkBotLongPoll', return_value=long_poll_listen_mock):
                bot = ChatBot('', '')
                bot.on_event = Mock()
                bot.run()

                bot.on_event.assert_called()
                bot.on_event.assert_any_call(obj)
                assert bot.on_event.call_count == count

    @isolate_db
    def test_run_ok(self):
        send_mock = Mock()
        api_mock = Mock()
        api_mock.users.get = Mock(return_value=self.USER_NAME)
        api_mock.messages.send = send_mock

        events = []
        for input_text in self.INPUTS:
            event = deepcopy(self.RAW_EVENT)
            event['object']['message']['text'] = input_text
            events.append(VkBotMessageEvent(event))

        long_poll_mock = Mock()
        long_poll_mock.listen = Mock(return_value=events)

        with patch('chatbot.vk_api.bot_longpoll.VkBotLongPoll', return_value=long_poll_mock):
            bot = ChatBot('', '')
            bot.api = api_mock
            bot.send_image = Mock()
            bot.registration = Mock()
            bot.api.users_name = api_mock.users.get
            bot.run()

        assert send_mock.call_count == len(self.INPUTS)

        real_outputs = []
        for call in send_mock.call_args_list:
            args, kwargs = call
            real_outputs.append(kwargs['message'])

        for real, expec in zip(real_outputs, self.EXPECTED_OUTPUTS):
            print(real)
            print('-' * 50)
            print(expec)
            print('-' * 50)
            print(real == expec)
            print('_' * 50)

        assert real_outputs == self.EXPECTED_OUTPUTS

    def test_image_generation(self):
        """
        Тест генерации билета.
        Сравнивает последовательность байтов с тестовым образцов.
        """
        with open('files/asd@asd.png', 'rb') as avatar_file:
            avatar_mock = Mock()
            avatar_mock.content = avatar_file.read()

        with patch('requests.get', return_value=avatar_mock):
            ticket = generate_ticket(name='Константин Суханкин', city_in='Париж', city_out='Москва',
                                     date='21-08-2020', passengers='4', user_id='12649692')

        assert ticket is not None
