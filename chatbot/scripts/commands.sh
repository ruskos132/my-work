# Запуск тестов в консоли:
python -m unittest

# coverage
coverage run --source=bot, handlers, settings -m unittest
coverage report -m

# create PostgresSQL database
create database vk_chat_bot;
psql -d vk_chat_bot

\l # список всех бах данных
\c vk_chat_bot # подключиться к базе
\dt # список отношений в выбранной дб
select * from userstates; # просмотреть бд
drop database vk_chat_bot; # удалить бд
