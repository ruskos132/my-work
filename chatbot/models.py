from pony.orm import Database, Required

from settings import DB_CONFIG

db = Database()
db.bind(**DB_CONFIG)


class UserStates(db.Entity):
    """Состояние пользователя внутри сценария"""
    user_id = Required(str, unique=True)
    scenario_name = Required(str)
    step_name = Required(str)
    name = Required(str)
    email = Required(str)
    city_out = Required(str)
    city_in = Required(str)
    date = Required(str)
    passengers = Required(str)
    comment = Required(str)


class Registration(db.Entity):
    """Заявка на регистрацию"""
    city_out = Required(str)
    city_in = Required(str)
    date = Required(str)
    name = Required(str)
    passengers = Required(str)


db.generate_mapping(create_tables=True)
