from io import BytesIO

import requests
from PIL import Image, ImageDraw, ImageFont


FORM_PATH = 'files/ticket.png'
FONT_PATH = 'font/Roboto-Medium.ttf'
AVATAR_SIZE = 235
AVATAR_POSITION = (2078, 543)


def font_sizer(size):
    """
    :param size: параметр, определюящий размер шрифта
    :return:PIL Object
    """
    font_with_size = ImageFont.truetype(FONT_PATH, size)
    return font_with_size


def generate_ticket(name, city_in, city_out, date, passengers, user_id):
    """
    :param name: Имя и фамилия пользователя
    :param city_in: Город назначения
    :param city_out: Город отправления
    :param date: Дата вылета
    :param passengers: Кол-во пассажиров
    :param user_id: Уникальный идентификатор пользователя
    :return: BytesIO() изображение в байтах, для отправки его в чат.
    """
    img = Image.open(FORM_PATH).convert('RGBA')

    # draw name
    draw = ImageDraw.Draw(img)
    draw.text((348, 270), text=name, font=font_sizer(60), fill=(0, 0, 0, 255))
    draw.text((1870, 233), text=name, font=font_sizer(40), fill=(0, 0, 0, 255))
    # draw date
    draw.text((490, 439), text=date, font=font_sizer(40), fill=(0, 0, 0, 255))
    draw.text((1797, 400), text=date, font=font_sizer(40), fill=(0, 0, 0, 255))
    # draw city out
    draw.text((920, 440), text=city_out, font=font_sizer(30), fill=(0, 0, 0, 255))
    draw.text((920, 530), text=city_in, font=font_sizer(30), fill=(0, 0, 0, 255))
    draw.text((1799, 660), text=city_out, font=font_sizer(25), fill=(0, 0, 0, 255))
    draw.text((1799, 765), text=city_in, font=font_sizer(25), fill=(0, 0, 0, 255))
    # draw passengers
    draw.text((1520, 320), text=passengers, font=font_sizer(40), fill=(0, 0, 0, 255))

    response = requests.get(url=f'https://api.adorable.io/avatars/{AVATAR_SIZE}/{user_id}')
    avatar_file_like = BytesIO(response.content)
    avatar = Image.open(avatar_file_like)
    img.paste(avatar, AVATAR_POSITION)

    ticket_for_user = BytesIO()
    img.save(ticket_for_user, 'png')
    ticket_for_user.seek(0)

    return ticket_for_user
