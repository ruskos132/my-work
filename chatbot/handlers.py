# -*- coding: utf-8 -*-
import re
from chatbot import *
import settings

re_name = re.compile(r'\w{3,15}[\s\-]\w{3,15}')
re_mail = re.compile(r"^\b[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\b")
re_date = re.compile(r"\d{2}[\-]\d{2}[\-]\d{4}")
re_telephone = re.compile(r"^(8|\+7)\d{10}")


def handler_city_out(text):
    for city in settings.LIST_OF_FLIGHTS[0]:
        city_check = city[:len(city) - 1]
        text_check = text[:len(city) - 1]
        if city_check.lower() == text_check.lower():
            return {'city_out': text.capitalize()}


def handler_city_in(text):
    for city in settings.LIST_OF_FLIGHTS[0]:
        city_check = city[:len(city) - 1]
        text_check = text[:len(city) - 1]
        if city_check.lower() == text_check.lower():
            return {'city_in': text.capitalize()}


def handler_date(text):
    match = re.match(re_date, text)
    if match:
        return {'date': text}


def select_date_handler(text):
    match = re.match(re_date, text)
    if match and match.string == text:
        return {'date': text}


def date_handler_in_choice(text):
    match = re.match(re_date, text)
    if match and match.string == text and text in chat_bot.FIVE_NEAR_DATE:
        return {'date': text}


def passengers_handler(text):
    if 1 <= int(text) <= 5:
        return {'passengers': text}


def comment(text):
    return {'comment': text}


def confirmation(text):
    match = text.lower()
    if match == 'да' or match == 'нет':
        return {'answer': match}


def telephone_number(text):
    match = re.match(re_telephone, text)
    if match:
        return {'telephone': text}


def handler_name(text):
    match = re.match(re_name, text)
    if match:
        return {'name': text}


def handler_email(text):
    matches = re.findall(re_mail, text)
    if len(matches) > 0:
        return {'email': text}
