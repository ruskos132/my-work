# -*- coding: utf-8 -*-


# Задача: вычислить 3 тикера с максимальной и 3 тикера с минимальной волатильностью в МНОГОПРОЦЕССНОМ стиле
#
# Бумаги с нулевой волатильностью вывести отдельно.
# Результаты вывести на консоль в виде:
#   Максимальная волатильность:
#       ТИКЕР1 - ХХХ.ХХ %
#       ТИКЕР2 - ХХХ.ХХ %
#       ТИКЕР3 - ХХХ.ХХ %
#   Минимальная волатильность:
#       ТИКЕР4 - ХХХ.ХХ %
#       ТИКЕР5 - ХХХ.ХХ %
#       ТИКЕР6 - ХХХ.ХХ %
#   Нулевая волатильность:
#       ТИКЕР7, ТИКЕР8, ТИКЕР9, ТИКЕР10, ТИКЕР11, ТИКЕР12
# Волатильности указывать в порядке убывания. Тикеры с нулевой волатильностью упорядочить по имени.
#
import multiprocessing
import operator
import os

nul_volatility_tickets = []
volatility_tickets = []
row_volatility_tickets = dict()


class Volatility(multiprocessing.Process):

    max_price = 0.0
    min_price = 0.0

    def __init__(self, ticket_name=None, collector=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ticket_name = ticket_name
        self.collector = collector

    def run(self):
        ticket_name, _ = self.ticket_name.split('.')
        with open(file=f'trades/{self.ticket_name}', mode='r', encoding='utf8') as row_file:
            row_file.readline()
            price = row_file.readline().split(',')
            self.max_price, self.min_price = float(price[2]), float(price[2])
            while True:
                line = row_file.readline().split(',')
                if line[0] != '':
                    self.min_max(line)
                else:
                    break

            average_price = (self.max_price + self.min_price) / 2
            volatility = ((self.max_price - self.min_price) / average_price) * 100

            row_volatility_tickets.update({ticket_name: volatility})

        self.collector.send(row_volatility_tickets)
        self.collector.close()

    def min_max(self, line):
        price = float(line[2])
        if price > self.max_price:
            self.max_price = price
        elif price < self.min_price:
            self.min_price = price


def get_file_names(row_file_names_path=None):
    list_file_names = []
    for list_file_name in row_file_names_path:
        for file_name in list_file_name[2]:
            list_file_names.append(file_name)
    return list_file_names


def sorting_volatility_tickets(tickets):
    for ticket in tickets:
        if ticket[1] != 0.0:
            volatility_tickets.append(ticket)
        else:
            nul_volatility_tickets.append(ticket[0])


row_file_path = os.path.realpath('trades')
file_path = os.path.normpath(row_file_path)
row_file_names = os.walk(file_path)


if __name__ == '__main__':
    file_names = get_file_names(row_file_names_path=row_file_names)
    pipes, files = [], []
    for name in file_names:
        parent_collector, child_collector = multiprocessing.Pipe()
        file = Volatility(ticket_name=name, collector=child_collector)
        pipes.append(parent_collector)
        files.append(file)

    for file in files:
        file.start()
    for pipe in pipes:
        row_volatility_tickets.update(pipe.recv())
    for file in files:
        file.join()

    sort_volatility_tickets = sorted(row_volatility_tickets.items(), key=operator.itemgetter(1), reverse=True)
    sorting_volatility_tickets(sort_volatility_tickets)

    print(

        f'Максимальная волатильность: \n'
        f'     {volatility_tickets[0][0]} - {round(volatility_tickets[0][1], 2):0<5} % \n'
        f'     {volatility_tickets[1][0]} - {round(volatility_tickets[1][1], 2):0<5} % \n'
        f'     {volatility_tickets[2][0]} - {round(volatility_tickets[2][1], 2):0<5} % \n'
        f'Минимальная волатильность: \n'
        f'     {volatility_tickets[-3][0]} - {round(volatility_tickets[-3][1], 2):0<5} % \n'
        f'     {volatility_tickets[-2][0]} - {round(volatility_tickets[-2][1], 2):0<5} % \n'
        f'     {volatility_tickets[-1][0]} - {round(volatility_tickets[-1][1], 2):0<5} % \n'

        f'Нулевая волатильность: \n'
        f'     {", ".join(nul_volatility_tickets)}'

    )
